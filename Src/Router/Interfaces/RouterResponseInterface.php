<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Interfaces;

/**
 * Interface RouterResponseInterface
 * 
 * @package Ds\Router\Interfaces
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
interface RouterResponseInterface
{
    /**
     * Returns current handler.
     * @return string|\Closure
     */
    public function getHandler();

    /**
     * Return Router Response Status Code.
     * @return int
     */
    public function getStatusCode();

    /**
     * Returns Route Names
     * @return array
     */
    public function getNames();

    /**
     * Returns any route variables that match route pattern.
     * @return array
     */
    public function getVars();
}
