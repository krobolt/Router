<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router;

use Ds\Router\Interfaces\RouteInterface;

/**
 * Route Class
 *
 * @package Ds\Router
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @see RouteInterface
 * @see RouteCollectionInterface
 */
class Route implements RouteInterface
{
    /**
     * Route Http Method
     *
     * @var string
     */
    protected $method;

    /**
     * Route Path Pattern
     *
     * @var string
     */
    protected $pattern;

    /**
     * Route Handler
     *
     * @var string|\Closure
     */
    protected $handler;

    /**
     * Route Names
     *
     * @var array $routeNames
     */
    protected $names;

    /**
     * Create a new route.
     *
     * @param string|array $method Route Http Method
     * @param string $pattern Route Path / Pattern
     * @param string|\Closure $handler Route Handler.
     * @param array $names Route Names
     */
    public function __construct($method, $pattern, $handler, array $names = [])
    {
        $this->method = $method;
        $this->pattern = $pattern;
        $this->handler = $handler;
        $this->names = $names;
    }

    /**
     * @inheritdoc
     */
    public function withMethod($method = '')
    {
        $new = clone $this;
        $new->method = (string)$method;
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withHandler($handler)
    {
        $new = clone $this;
        $new->handler = $handler;
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withPattern($pattern = '')
    {
        $new = clone $this;
        $new->pattern = (string)$pattern;
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function withNames(array $name = [])
    {
        $new = clone $this;
        $new->names = $name;
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return (string)$this->method;
    }

    /**
     * @inheritdoc
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @inheritdoc
     */
    public function getHandlerType()
    {
        return \gettype($this->handler);
    }

    /**
     * @inheritdoc
     */
    public function getPattern()
    {
        return (string)$this->pattern;
    }

    /**
     * @inheritdoc
     */
    public function getNames()
    {
        return (array)$this->names;
    }
}
