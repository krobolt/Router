<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router;

use Ds\Router\Adaptor\FastRouteAdaptor;
use Ds\Router\Interfaces\AdaptorInterface;
use Ds\Router\Interfaces\RouteCollectionInterface;
use Ds\Router\Serializer\SuperClosure;
use SuperClosure\Serializer;

/**
 * Class RouterFactory
 *
 * Factory helper class for creating Routers.
 *
 * @package Ds\Router
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class RouterFactory
{
    /**
     * @param AdaptorInterface $adaptor
     * @param RouteCollectionInterface $collection
     * @param array $options
     *
     * @return Router
     */
    public static function createRouter(AdaptorInterface $adaptor, RouteCollectionInterface $collection, array $options = [])
    {
        return new Router($adaptor, $collection);
    }

    /**
     * @param array $options
     * @return Router
     * @throws \Rs\Router\Exceptions\RouterException
     */
    public static function createFastRouter(array $options = [])
    {
        return new Router(
            new FastRouteAdaptor(
                new SuperClosure(
                    new Serializer()
                ), $options
            ),
            new RouteCollection(),
            $options
        );
    }
}
