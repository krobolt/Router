<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Loaders;

use Ds\Router\Exceptions\RouterException;
use Ds\Router\Interfaces\LoaderInterface;
use Ds\Router\Interfaces\RouteCollectionInterface;
use Ds\Router\Interfaces\RouterInterface;

/**
 * Abstract Loader
 *
 * @package Ds\Router\Loaders
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
abstract class AbstractLoader implements LoaderInterface
{
    /**
     * used to invalidate cache.
     * @var array|null
     */
    protected $signature;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @param array $files
     */
    public function createCacheSignature(array $files){
        $signature = array_map(function($v){
            if (!file_exists($v)){
                return 0;
            }
            return filesize($v);
        },$files);
        $this->signature = md5(implode('',$signature));
    }

    /**
     * Load Route File.
     *
     * @param string $file Route filename.
     *
     * @return RouteCollectionInterface
     * @throws RouterException
     */
    abstract public function loadFile(string $file);

    /**
     * Load multiple route files.
     *
     * @param array $files
     *
     * @return RouterInterface
     * @throws RouterException
     */
    abstract public function loadFiles(array $files);

    /**
     * With Router.
     *
     * @param RouterInterface $router
     * @return LoaderInterface
     */
    public function withRouter(RouterInterface $router)
    {
        $new = clone $this;
        $new->router = $router;
        return $new;
    }

    /**
     * Get Router.
     * @return RouterInterface
     */
    public function getRouter()
    {
        return $this->router;
    }
}
