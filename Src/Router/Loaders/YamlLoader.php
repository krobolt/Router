<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Loaders;

use Ds\Router\Exceptions\RouterException;
use Ds\Router\Interfaces\RouterInterface;
use Ds\Router\RouteCollection;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser;

/**
 * Class YamlLoader
 *
 * @package Ds\Router\Loaders
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class YamlLoader extends AbstractLoader
{
    /**
     * YamlLoader constructor.
     *
     * @param RouterInterface $router Router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @inheritdoc
     */
    public function loadFiles(array $files)
    {
        $this->signature = $this->signature ?? $this->createCacheSignature($files);

        if (!$this->router->isCached($this->signature)) {
            foreach ((array)$files as $filename) {
                $this->router = $this->router->mergeCollection($this->loadFile($filename));
            }
        }
        return $this->router;
    }

    /**
     * @inheritdoc
     */
    public function loadFile(string $file)
    {
        $this->signature = $this->signature ?? $this->createCacheSignature([$file]);

        if (!$this->router->isCached($this->signature)) {
            if (!\file_exists($file)) {
                throw new RouterException('Unable to locate file: ' . $file);
            }

            $collection = new RouteCollection();

            try {
                $yamlRead = new Parser();
                $routes = $yamlRead->parse(\file_get_contents($file));
                foreach ((array)$routes as $route) {
                    foreach ((array)$route['method'] as $routeMethod) {
                        $collection->addRoute($routeMethod, $route['path'], $route['handler'], $route['names']);
                    }
                }
            } catch (ParseException $e) {
                throw new RouterException($e->getMessage());
            }

            return $collection;
        }
        return $this->router->getCollection();
    }
}
