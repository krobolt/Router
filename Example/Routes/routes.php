<?php

/**
 * ROUTES
 * PHP ROUTE FILE
 *
 * Load Files via
 * App:loadFile(string $filename, array $accessibleVars)
 * App:loadFiles(array $filesnames, array $accessibleVars)
 */

/**
 * Route Global Vars
 * Variables made accessible from App::loadFile/s: $app, $bar @see example.php
 * These values will update regardless of cache.
 */

/**
 * Locale Route Vars.
 * Only available on this page.
 * If route cache is enabled then these values will be hardcoded to the route.
 * e.g. as long as the cache is valid $something will remain __FOO__ even if in the route.
 */

$collection = new \Rs\Router\RouteCollection();
$something = $bar;

$collection->addRoute('GET', '/path', 'myClass::myMethod', ['name']);
$collection->addRoute('GET', '/another', 'another-handler', ['name']);

$collection->group('/newPath', function () use ($collection, $something) {

    //Add both POST and GET routes with Closure.
    $collection->addRoute(['GET', 'POST'], '/new', function ($request) use ($something) {
        return $something;
    }, ['name']);

}, ['foo']);

$collection->addRoute('GET', '/mypath', 'handler::string', ['name']);

$collection->addRoute('GET', '/someproduct', function ($request) {
    return 'this is my response for ';
}, ['name']);

return $collection;
