<?php

//1) Creating the Router:
//a) Creating the component manually:
$fastRouteOptions = [
    'cacheDisabled' => false,
    'cacheFile' => __DIR__ . '/routes.cache',
    'cacheExpires' => 60 * 60 * 24 * 7,
    'errorHandlers' => [
        'default' => [
            'handler' => '\Site\Controllers\Error\ErrorController::error404',
            'name' => ['error']
        ]
    ]
];
$router = new Ds\Router\Router(
    new Ds\Router\Adaptor\FastRouteAdaptor(
        new Ds\Router\Serializer\SuperClosure(
            new SuperClosure\Serializer()
        ),
        $fastRouteOptions
    ), new Ds\Router\RouteCollection()
);

//The above is the same as using the factory helper:
$router = \Ds\Router\RouterFactory::createFastRouter($fastRouteOptions);


//Routes collections can then be added to the router:
$routeCollection = new \Ds\Router\RouteCollection();
$routeCollection->addRoute('GET','/some-path','handler::string',['routeName']);
$router->withCollection($routeCollection);

$app = new \StdClass();

//Or routes can be added via seperate files via the FileLoader
//Variables can be made accessible via 'vars' option.
$fileLoader = new Ds\Router\Loaders\FileLoader($router, [
    'vars' => [
        'app' => $app,
    ]
]);

//load routes from Routes/routes.php and Routes/routes2.php
$router = $fileLoader->loadFiles([
    __DIR__ . '/Routes/routes.php',
    __DIR__ . '/Routes/routes2.php'
]);

//Or routes can be added via yaml via the YamlLoader
$yamlLoader = new \Ds\Router\Loaders\YamlLoader($router);
$router = $yamlLoader->loadFiles([__DIR__ . '/Routes/routes.yml']);


//Get response handler for the request.
$response = $router->getRouterResponseFromPath('GET', '/newPath/new');

//Load Dispatcher to convert handler intro response.
$dispatcher = new \Ds\Router\Dispatcher\Dispatcher([
    'whitelist' => ['ControllerInterface'],
    'blacklist' => ['ControllerInterface2'],
    'autoWire' => true
]);

//Dispatch the request with params to pass to controller.
$dispatched = $dispatcher->dispatch(
    $serverRequest,
    $response->getHandler(),
        [
        'container' => '-my-container-class'
    ]
);

var_dump($dispatched->getBody()->getContents());
