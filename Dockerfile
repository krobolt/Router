FROM dsarchy/gitlab-php7-xdebug

ADD ./ /var/www/Src
WORKDIR /var/www/Src

RUN composer install && vendor/bin/phpunit --configuration phpunit.xml --coverage-text