[![Code Climate](https://codeclimate.com/github/djsmithme/Router/badges/gpa.svg)](https://codeclimate.com/github/djsmithme/Router)
[![Test Coverage](https://codeclimate.com/github/djsmithme/Router/badges/coverage.svg)](https://codeclimate.com/github/djsmithme/Router/coverage)

[![Build Status](https://travis-ci.org/djsmithme/Router.svg?branch=master)](https://travis-ci.org/djsmithme/Router)

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/djsmithme/Router/badges/quality-score.png?branch=master)](https://scrutinizer-ci.com/g/djsmithme/Router/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/g/djsmithme/Router/badges/coverage.png?branch=master)](https://scrutinizer-ci.com/g/djsmithme/Router/?branch=master)
[![Build Status](https://scrutinizer-ci.com/g/djsmithme/Router/badges/build.png?branch=master)](https://scrutinizer-ci.com/g/djsmithme/Router/build-status/master)



# Rs\Router

PHP Router

Adaptor Based Routing Component.

Includes the following Adaptors:
 * Fast Route (with cached routes for String|Closure)
 * Symfony Routing


## Creating the Router


    $router = new Rs\Router\Router(
        Ds\Router\Interfaces\AdaptorInterface,
        Ds\Router\Interfaces\RouteCollectionInterface
    )

    //Fast Router with Serialized Routes.
    $router = new Router(
        new FastRouteAdaptor(
            new SuperClosure(
                new Serializer()
            ),
            (array) $options
        ),
        new RouteCollection()
    );

    //Use the Factory methods for easy access
    $router = \Ds\Router\RouterFactory::createFastRouter($options);


### Router Adaptors

Adaptors must implement 'Ds\Router\Interfaces\AdaptorInterface

Provided Adaptors:
 * FastRoute - Ds\Adaptors\FastRouteAdaptor
 * Symfony - Ds\Adaptors\SymfonyAdaptor

```
    Ds\Router\Adaptor\FastRouteAdaptor(
        SerializerInterface $serializer,
        array $options = []
    );


    $fastRoute = new FastRouteAdaptor(
         new SuperClosure(
            new Serializer()
         ),
         [
             'cacheDisabled' => FALSE,
             'cacheFile' => __DIR__ . '/routes.cache',
             'cacheExpires' => 60 * 60 * 24 * 7,
             'errorHandlers' => [
                 'default' => [
                     'handler' => '\Site\Controllers\Error\ErrorController::error404',
                     'name' => ['error']
                 ]
             ]
         ]
    )

$router = $router->withAdaptor($fastRoute)

```


### Route Collections

Routes Collections must implement \Ds\Router\Interfaces\RouteCollectionInterface.

```
$routeCollection = new \Ds\Router\RouteCollection();

$routeCollection->group('/sub-dir', function() use ($routeCollection){

    $routeCollection->addRoute(
            (string)$httpMethod,
            (string)$path,
            (string)$handler,
            (array)$names
    );
});

$router = $router->withCollection($routeCollection)

```

### Loaders

Collection Loaders must implement \Ds\Router\Interfaces\LoaderInterface.

```
//todo: docs on how to use file/yaml loaders

```

## Using the router.

Once the adaptor and routes have been added Routes can be matched to the PSR7 request and a RS\Router\RouterResponse returned


### Dispatching Routes.

Use Router::getRouterResponse() to return the RouterResponse for that request.

```
```

### Dispatching Handlers.

Handlers must be resolved from the in order to get the response from the controller/closure. This is useful when dealing with expensive calls or to areas of the site than might not be accessible before/after
middleware e.g. Authentication. Trying RouterResponse::getResponse() without resolving will cause an Exception to be thrown.

```
```