<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router\Serializer;

use Ds\Router\Interfaces\SerializerInterface;
use Ds\Router\Serializer\SuperClosure;
use SuperClosure\Serializer;

/**
 * Class SuperClosureTest
 * @package Tests\Ds\Router\Serializer
 */
class SuperClosureTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var SerializerInterface
     */
    public $serializer;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\SuperClosure\Serializer
     */
    public $superClosure;

    /**
     *
     */
    public function setUp()
    {
        $this->superClosure = $this->getMockBuilder(Serializer::class)->getMock();
        $this->serializer = new SuperClosure($this->superClosure);
    }

    /**
     *
     */
    public function testSerializeCalled()
    {
        $data = function () {
            return true;
        };

        $this->superClosure
            ->expects($this->once())
            ->method('serialize')
            ->with(
                $this->equalTo($data)
            );
        $this->serializer->serialize($data);
    }

    /**
     *
     */
    public function testSerializeNonClosure()
    {
        $this->setExpectedException(\Exception::class);
        $data = $this->superClosure;
        $this->serializer->serialize($data);
    }

    /**
     *
     */
    public function testUnserializeCalled()
    {
        $data = 'some-string';
        $this->superClosure
            ->expects($this->once())
            ->method('unserialize');
        $this->serializer->unserialize($data);
    }
}
