<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router;

use Ds\Router\Route;

/**
 * Class RouteTest
 * @package Tests\Ds\Router
 */
class RouteTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Route
     */
    public $route;

    /**
     * @var string
     */
    protected $expectedMethod = 'GET';

    /**
     * @var string
     */
    protected $expectedPattern = '/path';

    /**
     * @var string
     */
    protected $expectedHandler = 'handler';

    /**
     * @var array
     */
    protected $expectedNames = ['name-1','name-2'];

    /**
     * Route setUp.
     */
    public function setUp()
    {
        $this->route = new Route(
            $this->expectedMethod,
            $this->expectedPattern,
            $this->expectedHandler,
            $this->expectedNames
        );
    }

    /**
     * Test immutable Route::withMethod()
     */
    public function testWithMethod()
    {
        $expected = 'POST';
        $route = $this->route->withMethod($expected);
        $this->assertSame(
            $expected,
            Helpers\Reflection::getProperty(Route::class, 'method', $route)
        );
    }

    /**
     * Test immutable Route::withPattern()
     */
    public function testWithPattern()
    {
        $expected = '/my-pattern/foo';
        $route = $this->route->withPattern($expected);
        $this->assertSame(
            $expected,
            Helpers\Reflection::getProperty(Route::class, 'pattern', $route)
        );
    }

    /**
     *
     */
    public function testWithNames()
    {
        $expected = ['foo','bar','baz'];
        $route = $this->route->withNames($expected);
        $this->assertSame(
            $expected,
            Helpers\Reflection::getProperty(Route::class, 'names', $route)
        );
    }

    /**
     *
     */
    public function testWithHandlerString()
    {
        $expected = 'controller::class';
        $route = $this->route->withHandler($expected);

        $this->assertSame(
            $expected,
            Helpers\Reflection::getProperty(Route::class, 'handler', $route)
        );
    }

    /**
     *
     */
    public function testWithHandlerClosure()
    {
        $expected = function () {
            return 'closure';
        };
        $route = $this->route->withHandler($expected);

        $actual = Helpers\Reflection::getProperty(Route::class, 'handler', $route);

        $this->assertSame(
            $expected,
            $actual
        );
    }

    /**
     *
     */
    public function testGetMethod()
    {
        $this->assertEquals($this->expectedMethod, $this->route->getMethod());
    }

    /**
     *
     */
    public function testGetHandler()
    {
        $this->assertEquals($this->expectedHandler, $this->route->getHandler());
    }

    /**
     *
     */
    public function testGetHandlerTypeString()
    {
        $expected = 'string';
        $actual = $this->route->getHandlerType();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testGetHandlerTypeClosure()
    {
        $closure = function () {
        };
        $expected = 'object';
        $route = $this->route->withHandler($closure);
        $actual = $route->getHandlerType();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testGetPattern()
    {
        $this->assertEquals($this->expectedPattern, $this->route->getPattern());
    }

    /**
     *
     */
    public function testGetNames()
    {
        $this->assertEquals($this->expectedNames, $this->route->getNames());
    }
}
