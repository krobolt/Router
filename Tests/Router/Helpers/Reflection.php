<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router\Helpers;

/**
 * Class Reflection
 *
 * PHPUnit Helper Class
 *
 * @package Tests\Ds\Router\Helpers
 */
class Reflection
{
    /**
     * Get private/protected properties from Reflection.
     * @param string $className
     * @param string $property
     * @param object $object
     * @return mixed
     */
    public static function getProperty($className, $property, $object)
    {
        $reflector = new \ReflectionClass($className);
        $prop = $reflector->getProperty($property);
        $prop->setAccessible(true);
        return $prop->getValue($object);
    }

    /**
     * Set private/protected properies and return original object.
     * @param string $className
     * @param string $property
     * @param object $object
     * @param mixed $value
     * @return object
     */
    public static function setProperty($className, $property, $object, $value)
    {
        $reflectionClass = new \ReflectionClass($className);
        $reflectionProperty = $reflectionClass->getProperty($property);
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($object, $value);
        return $object;
    }
}
