<?php

/**
 * PHP ROUTE FILE for Tests
 */

$collection = new \Ds\Router\RouteCollection();

$collection->addRoute('GET', '/pathalt', function () use ($variable) {
    return $variable;
}, ['name']);

return $collection;
