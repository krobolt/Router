<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router\Loaders;

use Ds\Router\Exceptions\RouterException;
use Ds\Router\Interfaces\RouteCollectionInterface;
use Ds\Router\Interfaces\RouterInterface;
use Ds\Router\Loaders\YamlLoader;
use Ds\Router\RouteCollection;

/**
 * Class YamlLoaderTest
 * @package Tests\Ds\Router\Loaders
 */
class YamlLoaderTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|RouterInterface
     */
    public $router;

    /**
     * @var YamlLoader
     */
    public $loader;

    /**
     * @var string
     */
    public $file;

    /**
     *
     */
    public function setUp()
    {
        $this->router = $this->getMockBuilder(RouterInterface::class)->getMock();
        $this->loader = new YamlLoader($this->router);
        $this->file = __DIR__ . '/Files/FileLoaderRoutes.yml';
    }

    /**
     *
     */
    public function testLoadFileNoCacheParse()
    {
        $this->setExpectedException(RouterException::class);


        $this->router->expects($this->once())
            ->method('isCached')
            ->willReturn(false);
        $this->loader->loadFile(__DIR__ . '/Files/malformed.yml');
    }

    /**
     *
     */
    public function testLoadFileNoCacheNoFile()
    {
        $this->setExpectedException(RouterException::class);

        $this->router->expects($this->once())
            ->method('isCached')
            ->willReturn(false);

        $this->loader->loadFile(__DIR__ . '/RouteFileNone.yml');
    }

    /**
     *
     */
    public function testLoadFileNoCache()
    {
        $this->router->expects($this->once())
            ->method('isCached')
            ->willReturn(false);

        $collection = $this->loader->loadFile($this->file);
        $route = $collection->current();

        $expected = 'myHandler::myMethod';
        $actual = $route->getHandler();

        $this->assertInstanceOf(RouteCollectionInterface::class, $collection);
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testLoadFileCache()
    {
        $this->router->expects($this->once())
            ->method('isCached')
            ->willReturn(true);

        $this->router->expects($this->once())
            ->method('getCollection')
            ->willReturn(new RouteCollection());

        $collection = $this->loader->loadFile($this->file);

        $this->assertInstanceOf(RouteCollection::class, $collection);
    }

    /**
     *
     */
    public function testLoadFilesNoCache()
    {
        $this->router->expects($this->any())
            ->method('isCached')
            ->willReturn(false);

        $this->router->expects($this->any())
            ->method('mergeCollection')
            ->willReturn($this->router);

        $this->loader->loadFiles([
            $this->file,
            __DIR__ . '/Files/FileLoaderRoutesAlt.yml'
        ]);
    }
}
