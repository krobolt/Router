<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router\Adaptor;

use Ds\Router\Interfaces\AdaptorInterface;
use Ds\Router\Interfaces\SerializerInterface;

/**
 * Class AbstractAdaptorTest
 * @package Tests\Ds\Router\Adaptor
 */
class AbstractAdaptorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AdaptorInterface
     */
    public $adaptor;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|SerializerInterface
     */
    public $serializer;

    /**
     *
     */
    public function setUp()
    {
        $this->serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $this->adaptor = new AbstractAdaptorMock($this->serializer);
    }

    /**
     *
     */
    public function testGetOptions()
    {
        $expected = [];
        $actual = $this->adaptor->getOptions();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testGetOption()
    {
        $expected = [];
        $actual = $this->adaptor->getOptions();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testWithOptions()
    {
        $expected = ['new' => 'options'];
        $this->adaptor->withOptions($expected);
        $actual = $this->adaptor->getOptions();
        $this->assertEquals($expected, $actual);
    }
}
