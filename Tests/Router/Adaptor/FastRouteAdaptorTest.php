<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router\Adaptor;

use Ds\Router\Adaptor\FastRouteAdaptor;
use Ds\Router\Exceptions\RouterException;
use Ds\Router\Interfaces\SerializerInterface;
use Tests\Ds\Router\Helpers\Reflection;

/**
 * Class FastRouteAdaptorTest
 * @package Tests\Ds\Router\Adaptor
 */
class FastRouteAdaptorTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var FastRouteAdaptor
     */
    public $fastRoute;
    /**
     * @var array
     */
    public $options;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|SerializerInterface
     */
    public $serializer;

    /**
     *
     */
    public function setUp()
    {
        $this->options = [
            'cacheDisabled' => true,
            'errorHandlers' => [
                'default' => [
                    'handler' => 'errorController::method404',
                    'name' => ['error']
                ]
            ]
        ];

        $this->serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $this->fastRoute = new FastRouteAdaptor($this->serializer, $this->options);
    }

    /**
     *
     */
    public function testConstructMissingOptions()
    {
        $this->setExpectedException(RouterException::class);
        return new FastRouteAdaptor($this->serializer, []);
    }

    /**
     *
     */
    public function testIsCachedWithCacheEnabled()
    {
        $expected = false;
        $adaptor = new FastRouteAdaptor($this->serializer, [
            'cacheDisabled' => $expected,
            'cacheFile' => __DIR__,
            'errorHandlers' => [
                'default' => [
                    'handler' => 'errorController::method404',
                    'name' => ['error']
                ]
            ]
        ]);

        $options = Reflection::getProperty(FastRouteAdaptor::class, 'options', $adaptor);
        $this->assertEquals($expected, $options['cacheDisabled']);
    }

    /**
     *
     */
    public function testMatch()
    {
    }

    /**
     *
     */
    public function testGetCachedRoutes()
    {
    }

    /**
     *
     */
    public function testIsCached()
    {
    }

    /**
     * Internal Methods
     */
    public function test_checkCacheExpires()
    {
    }

    /**
     *
     */
    public function test_getCachedDispatcher()
    {
    }

    /**
     *
     */
    public function test_createFastRouteHandler()
    {
    }

    /**
     *
     */
    public function test_findOptionsHandler()
    {
    }

    /**
     *
     */
    public function test_createRouterResponse()
    {
    }
}
