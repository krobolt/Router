<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router;

use Ds\Router\RouterResponse;

/**
 * Class RouterResponseTest
 * @package Tests\Ds\Router
 */
class RouterResponseTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var int
     */
    public $code;

    /**
     * @var string|\Closure
     */
    public $handler;

    /**
     * @var array
     */
    public $names;

    /**
     * @var array
     */
    public $vars;

    /**
     * @var RouterResponse
     */
    public $routerResponse;

    /**
     * RouterResponse setUp.
     */
    public function setUp()
    {
        $this->handler = 'handler';
        $this->code = 200;
        $this->names = ['name','name2'];
        $this->vars = ['a' => 1, 'b' =>2];
        $this->routerResponse = new RouterResponse($this->code, $this->handler, $this->names, $this->vars);
    }

    /**
     *
     */
    public function testGetStatusCode()
    {
        $this->assertEquals($this->code, $this->routerResponse->getStatusCode());
    }

    /**
     *
     */
    public function testGetHandler()
    {
        $this->assertEquals($this->handler, $this->routerResponse->getHandler());
    }

    /**
     *
     */
    public function testGetVars()
    {
        $this->assertEquals($this->vars, $this->routerResponse->getVars());
    }

    /**
     *
     */
    public function testGetNames()
    {
        $this->assertEquals($this->names, $this->routerResponse->getNames());
    }
}
